<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="page-wrapper">
	<div class="row">
	    <div class="col-lg-12">
			<h4 class="page-header">${title}</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					${title} 등록
				</div>
				<div class="panel-body">
					<div class="row">
						 <div class="col-lg-12">
						 	<form name="voteForm" id="voteForm" method="post">
						 	<c:forEach begin="1" end="5" varStatus="index">
					 		<div class="form-group">
					 			<label>${title} 후보자${index.count}</label>
					 			<input class="form-control" type="text" name="name" />
					 		</div>
						 	</c:forEach>
					 		<p>
					 			<button type="submit" class="btn btn-primary">등록</button>
					 		</p>
						 	</form>
						 </div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					${title} 히스토리
				</div>
				<div class="panel-body">
					<!-- <div id="people"> -->
					<ul id="people" class="people">
					<c:if test="${fn:length(list) > 0}">
						<c:forEach items="${list}" var="vote">
							<li>${vote.groupId} | ${vote.name } | ${vote.regDt} <a href="javascript:updateVote('${vote.id}', '${vote.name}');" class="btn btn-warning btn-xs">수정</a> <a href="javascript:deleteVote(${vote.id});" class="btn btn-danger btn-xs">삭제</a></li>
						</c:forEach>
					</c:if>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="update">
<form id="updateForm" method="post">
	<input type="hidden" name="id" id="id" />
	<input type="text" name="name" id="updNm" />
	<button type="submit">수정</button>
</form>
</div>
<script type="text/javascript">
$(function() {
	$('#voteForm').submit(function(e) {
		e.preventDefault();
		if(confirm('등록하시겠습니까?')) {
			$.post('/vote/${type}/vote.do', $('#voteForm').serialize(), function(data) {
				var tag = '';
				
				$.each(data.list, function(index, value) {
					tag += '<li>'+ value.groupId +' | '+ value.name +' | '+ value.regDt +' <a href="javascript:updateVote(\''+ value.id +'\',\''+ value.name +'\');" class="btn btn-warning btn-xs">수정</a> <a href="javascript:deleteVote('+ value.id +');" class="btn btn-danger btn-xs">삭제</a></li>';
				});
				$('#people').prepend(tag);
				
				$('input[name="name"]').val('');
			});			
		}
	});
	
	$('#updateForm').submit(function(e) {
		e.preventDefault();
		if(confirm('수정하시겠습니까?')) {
			$.post('/vote/${type}/updateVote.do', $('#updateForm').serialize(), function(data) {
				if(data) {
					alert('수정되었습니다.');
					location.href = '/vote/${type}/vote.do';
				}
			});
		}
	});
});
</script>
<script type="text/javascript">
	function updateVote(id, name) {
		$('#id').val(id);
		$('#updNm').val(name);
		$.blockUI({ message: $('#updateForm') });
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
	}
	
	function deleteVote(id) {
		if(confirm('삭제하시겠습니까?')) {
			var param = {'id': id};
			$.post('/vote/{type}/deleteVote.do', param, function(data) {
				if(data) {
					alert('삭제되었습니다.');
					location.href = '/vote/${type}/vote.do';
				}
			});
		}		
	}
	
	function objToString (obj) {
	    var str = '';
	    for (var p in obj) {
	        if (obj.hasOwnProperty(p)) {
	            str += p + '::' + obj[p] + '\n';
	        }
	    }
	    return str;
	}
</script>