<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="/dashboard.do" ${type eq '00' ? 'class="active"' : ''}><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <sec:authorize access="hasRole('ROLE_BROADCAST')">
            <li>
            	<a href="/chart/01/chart.do" ${type eq '01' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 장로 예비 후보자 차트</a>
            </li>
            <li>
            	<a href="/chart/02/chart.do" ${type eq '02' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 장로 차트</a>
            </li>
            <li>
            	<a href="/chart/03/chart.do" ${type eq '03' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 안수집사 예비 후보자 차트</a>
            </li>
            <li>
            	<a href="/chart/04/chart.do" ${type eq '04' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 안수집사 차트</a>
            </li>
            <%-- <li>
            	<a href="/chart/05/chart.do" ${type eq '05' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 권사 예비 후보자 차트</a>
            </li>
            <li>
            	<a href="/chart/06/chart.do" ${type eq '06' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 권사 차트</a>
            </li> --%>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_MANAGER')">
            <li>
            	<a href="/vote/01/vote.do" ${type eq '01' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 장로 예비 후보자 투표</a>
            </li>
            <li>
            	<a href="/vote/02/vote.do" ${type eq '02' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 장로 투표</a>
            </li>
            <li>
            	<a href="/vote/03/vote.do" ${type eq '03' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 안수집사 예비 후보자 투표</a>
            </li>
            <li>
            	<a href="/vote/04/vote.do" ${type eq '04' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 안수집사 투표</a>
            </li>
            <%-- <li>
            	<a href="/vote/05/vote.do" ${type eq '05' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 권사 예비 후보자 투표</a>
            </li>
            <li>
            	<a href="/vote/06/vote.do" ${type eq '06' ? 'class="active"' : ''}><i class="fa fa-check-square"></i> 권사 투표</a>
            </li> --%>
            </sec:authorize>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>