<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>흰돌교회 투표 시스템</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    <link href="/css/vote.css" rel="stylesheet">
    <link href="/css/morris.css" rel="stylesheet">
    <link href="/css/plugins/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/jquery-ui.min.css" type="text/css">
	<script src="/js/jquery-2.1.1.min.js"></script>
	<script src="/js/jquery.blockUI.js"></script>
	<script src="/js/highcharts.js"></script>
	<script src="/js/modules/data.js"></script>
	<script src="/js/modules/drilldown.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
</head>
<body>
<div id="wrapper">
	<!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="leftmenu" />
	</nav>
	<tiles:insertAttribute name="content" />
</div>
</body>
</html>
