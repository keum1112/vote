<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h4 class="page-header">Dashboard</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">500</div>
							<div>총 개수</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${openCount}</div>
							<div>개표수</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
var categories = new Array();

$(function() {
	var options = {
		title: {
			text: '장로 예비 후보자 차트'
		},
		xAxis: {
			categories: categories,
		},
        yAxis: {
            title: {
                text: '개표수'
            }
        },
        chart: {
            renderTo: 'container',
            type: 'column'
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        series: [{}]
    };
	
	var param = {type: '${type}'};
	$.post('/chart/{type}/getChartData.do', param, function(result) {
		$.each(result.data, function(index, value) {
			categories.push(value.name);	
		});
		
		options.series[0].data = result.data;
        var chart = new Highcharts.Chart(options);
	});
});
window.setTimeout('window.location.reload()',10000);
</script>