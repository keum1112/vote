<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>흰돌교회 투표 시스템</title>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/sb-admin-2.css" rel="stylesheet">
</head>
<body>
<div class="container">
	<div class="row">
    	<div class="col-md-4 col-md-offset-4">
        	<div class="login-panel panel panel-default">
            	<div class="panel-heading">
            		<h3>흰돌교회 투표 시스템</h3>
				</div>
                <div class="panel-body">
                	<form action="<c:url value="j_spring_security_check" />" method="post">
                    	<fieldset>
							<div class="form-group">
                            	<input class="form-control" placeholder="아이디" name="j_username" type="text" autofocus>
							</div>
                            <div class="form-group">
                            	<input class="form-control" placeholder="비밀번호" name="j_password" type="password" value="">
							</div>
							<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>