package vote.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import vote.domain.SessionUser;

public class VoteUserDetailsService implements UserDetailsService {
	
	@Autowired SqlSession sqlSession;

	@SuppressWarnings("rawtypes")
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		SessionUser user = null;
		
		// 유저정보
		Map userInfo = sqlSession.selectOne("sign.selectAuth", username);
		
		if(userInfo != null) {
			// 권한정보
			List<GrantedAuthority> authorities = getAuthorization(userInfo);
			
			int id = Integer.parseInt(userInfo.get("id").toString());
			String password = userInfo.get("password").toString();
			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			
			user = new SessionUser(username, password, enabled, accountNonExpired, credentialsNonExpired,
					accountNonLocked, authorities, id);
		}
		
		return user;
	}
	
	/**
	 * 권한부여
	 * @param userAuths 권한코드값들
	 * @return List<GrantedAuthority> casting
	 */
	@SuppressWarnings("rawtypes")
	private List<GrantedAuthority> getAuthorization(Map userInfo) {
		List<GrantedAuthority> aurhorities = new ArrayList<GrantedAuthority>();
		aurhorities.add(new SimpleGrantedAuthority(userInfo.get("role").toString()));
		return aurhorities;
	}

}
