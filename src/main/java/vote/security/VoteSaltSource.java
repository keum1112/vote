package vote.security;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetails;

public class VoteSaltSource implements SaltSource {
	
	@Autowired SqlSession sqlSession;

	@Override
	public Object getSalt(UserDetails user) {
		return sqlSession.selectOne("sign.selectSalt", user.getUsername());
	}

}
