package vote.dao;

import java.sql.SQLException;
import java.util.List;

public interface GenericDao<E, K> {
	
	public void create(E e) throws SQLException;
	
	public List<E> list(E e) throws SQLException;
	
	public E detail(K k) throws SQLException;
	
	public void update(E e) throws SQLException;
	
	public void delete(K k) throws SQLException;

}
