package vote.dao;

import java.sql.SQLException;
import java.util.List;

import vote.domain.Vote;

public interface VoteDao extends GenericDao<Vote, Integer> {
	
	public Vote detail(Vote vote) throws SQLException;
	
	/**
	 * 최대 GroupId 값 구하기
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public int selectMaxGroupId(String type) throws SQLException;
	
	/**
	 * 입력한 Group List 구하기
	 * @param type
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public List<Vote> groupList(String type, int groupId) throws SQLException;
	
	/**
	 * 자동완성
	 * @param vote
	 * @return
	 * @throws SQLException
	 */
	public List<String> selectNameList(Vote vote) throws SQLException;
	
}
