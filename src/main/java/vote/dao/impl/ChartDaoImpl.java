package vote.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import vote.dao.ChartDao;
import vote.domain.Chart;

@Repository("chartDao")
public class ChartDaoImpl extends GenericDaoImpl<Chart, Integer> implements ChartDao {

	@Override
	public void create(Chart e) throws SQLException {
	}

	@Override
	public List<Chart> list(Chart chart) throws SQLException {
		return null;
	}

	@Override
	public Chart detail(Integer k) throws SQLException {
		return null;
	}

	@Override
	public void update(Chart e) throws SQLException {
	}

	@Override
	public void delete(Integer k) throws SQLException {
	}

	@Override
	public List<Chart> list(String type) throws SQLException {
		return sqlSession.selectList("chart.selectChartList", type);
	}

	@Override
	public int openCount(String type) throws SQLException {
		return sqlSession.selectOne("chart.selectOpenCount", type);
	}

}
