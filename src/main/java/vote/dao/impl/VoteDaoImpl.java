package vote.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import vote.dao.VoteDao;
import vote.domain.SessionUser;
import vote.domain.Vote;
import vote.util.SessionUtil;

@Repository(value="voteDao")
public class VoteDaoImpl extends GenericDaoImpl<Vote, Integer> implements VoteDao {
	
	@Autowired SqlSession sqlSession;
	
	@Override
	public void create(Vote vote) throws SQLException {
		sqlSession.insert("vote.insertVote", vote);
	}

	@Override
	public List<Vote> list(Vote vote) throws SQLException {
		return sqlSession.selectList("vote.selectVoteList", vote);
	}

	@Override
	public Vote detail(Integer k) throws SQLException {
		return null;
	}
	
	@Override
	public Vote detail(Vote vote) throws SQLException {
		return sqlSession.selectOne("vote.selectVote", vote);
	}
	
	@Override
	public void update(Vote vote) throws SQLException {
		sqlSession.update("vote.updateVote", vote);
	}

	@Override
	public void delete(Integer id) throws SQLException {
		sqlSession.delete("vote.deleteVote", id);
	}

	@Override
	public int selectMaxGroupId(String type) throws SQLException {
		return sqlSession.selectOne("vote.selectMaxGroupId", type);
	}

	@Override
	public List<Vote> groupList(String type, int groupId) throws SQLException {
		Vote vote = new Vote();
		SessionUser user = SessionUtil.getUser();
		
		vote.setType(type);
		vote.setGroupId(groupId);
		vote.setRegId(user.getId());
		
		return sqlSession.selectList("vote.groupList", vote);
	}

	@Override
	public List<String> selectNameList(Vote vote) throws SQLException {
		return null;
	}

	
}
