package vote.dao;

import java.sql.SQLException;
import java.util.List;

import vote.domain.Chart;

public interface ChartDao extends GenericDao<Chart, Integer> {
	public List<Chart> list(String type) throws SQLException;
	
	public int openCount(String type) throws SQLException;
}
