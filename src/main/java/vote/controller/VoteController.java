package vote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import vote.dao.VoteDao;
import vote.domain.SessionUser;
import vote.domain.Vote;
import vote.service.VoteService;
import vote.util.SessionUtil;

@Controller
public class VoteController {
	
	@Autowired VoteService voteService;
	@Autowired VoteDao voteDao;

	/**
	 * 투표 등록폼
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vote/{type}/vote.do", method = RequestMethod.GET)
	@Secured("ROLE_MANAGER")
	public ModelAndView voteForm(@PathVariable String type, Vote vote) throws Exception {
		ModelAndView mav = new ModelAndView();
		SessionUser user = SessionUtil.getUser();
		
		String title = null;
		
		if(type.equals("01"))		title = "장로 예비 후보자 투표";
		else if(type.equals("02"))	title = "장로 투표";
		else if(type.equals("03"))	title = "안수집사 예비 후보자 투표";
		else if(type.equals("04"))	title = "안수집사 투표";
		else if(type.equals("05"))	title = "권사 예비 후보자 투표";
		else if(type.equals("06"))	title = "권사 투표";
		
		vote.setType(type);
		vote.setRegId(user.getId());
		
		List<Vote> list = voteDao.list(vote);
		
		mav.addObject("type", type);
		mav.addObject("title", title);
		mav.addObject("list", list);
		
		mav.setViewName("vote/"+ type +"/voteForm.tiles");
		return mav;
	}

	/**
	 * 투표 등록
	 * @param type
	 * @param voteParam
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vote/{type}/vote.do", method = RequestMethod.POST)
	@ResponseBody
	@Secured("ROLE_MANAGER")
	public ModelMap vote(@PathVariable String type, HttpServletRequest request) throws Exception {
		ModelMap mm = new ModelMap();
		String[] names = request.getParameterValues("name");
		List<Vote> list = voteService.vote(type, names);
		
		int groupId = ((Vote) list.get(0)).getGroupId();
		
		mm.addAttribute("list", list);
		mm.addAttribute("groupId", groupId);
		
		return mm;
	}
	
	/**
	 * 투표 수정
	 * @param type
	 * @param vote
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vote/{type}/updateVote.do", method = RequestMethod.POST)
	@ResponseBody
	@Secured("ROLE_MANAGER")
	public boolean updateVote(@PathVariable String type, Vote vote) throws Exception {
		boolean result = false;
		voteDao.update(vote);
		result = true;
		return result;
	}

	/**
	 * 투표 삭제
	 * @param type
	 * @param vote
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/vote/{type}/deleteVote.do", method = RequestMethod.POST)
	@ResponseBody
	@Secured("ROLE_MANAGER")
	public boolean deleteVote(@PathVariable String type, Vote vote) throws Exception {
		boolean result = false;
		voteDao.delete(vote.getId());
		result = true;
		return result;
	}
}
