package vote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import vote.dao.ChartDao;

@Controller
public class ChartController {
	
	@Autowired ChartDao chartDao;

	@RequestMapping(value="/chart/{type}/chart.do", method = RequestMethod.GET)
	@Secured("ROLE_BROADCAST")
	public ModelAndView voteForm(@PathVariable String type) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("type", type);
		mav.addObject("openCount", chartDao.openCount(type));
		
		mav.setViewName("chart/chartForm.tiles");
		return mav;
	}
	
	@RequestMapping(value="/chart/{type}/getChartData.do", method = RequestMethod.POST)
	@Secured("ROLE_BROADCAST")
	@ResponseBody
	public ModelMap getChart(@RequestParam("type") String type) throws Exception {
		ModelMap mm = new ModelMap();
		mm.addAttribute("data", chartDao.list(type));
		return mm;
	}
	
}
