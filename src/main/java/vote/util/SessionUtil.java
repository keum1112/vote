package vote.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import vote.domain.SessionUser;

public class SessionUtil {

	public static SessionUser getUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!auth.getPrincipal().equals("anonymousUser")) {
			return (SessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		return null;
	}
	
}
