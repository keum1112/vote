package vote.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vote.dao.VoteDao;
import vote.domain.SessionUser;
import vote.domain.Vote;
import vote.service.VoteService;
import vote.util.SessionUtil;

@Service("voteService")
public class VoteServiceImpl implements VoteService {
	
	@Autowired VoteDao voteDao;

	@Override
	@Transactional
	public List<Vote> vote(String type, String[] names) throws Exception {
		SessionUser user = SessionUtil.getUser();
		
		int groupId = voteDao.selectMaxGroupId(type);
		
		for(String name : names) {
			if(!name.isEmpty()) {
				Vote vote = new Vote();
				
				vote.setType(type);
				vote.setName(name);
				vote.setRegId(user.getId());
				vote.setGroupId(groupId);
	
				voteDao.create(vote);
			}
		}
		
		List<Vote> result = voteDao.groupList(type, groupId);
		
		return result;
	}

}
