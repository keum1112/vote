package vote.service;

import java.util.List;

import vote.domain.Vote;

public interface VoteService {

	public List<Vote> vote(String type, String[] names) throws Exception;
	
}
