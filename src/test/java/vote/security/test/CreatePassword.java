package vote.security.test;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations = { "classpath:spring/spring-security.xml",
		"classpath:spring/spring-config.xml",
		"classpath:spring/spring-database.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CreatePassword {

	@Autowired ShaPasswordEncoder shaPasswordEncoder;
	
	@Test
	public void createPassword2() {
		String salt = RandomStringUtils.random(10, true, true);
		String password = "test123";
		String encodedPassword = shaPasswordEncoder.encodePassword(password, salt);
		
		System.out.println("SALT : "  + salt);
		System.out.println("Password : "  + password);
		System.out.println("EncodedPassword : " + encodedPassword);
		System.out.println(encodedPassword.length());
	}
	
}
